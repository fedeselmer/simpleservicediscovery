/* 
 * Copyright 2014 BnearIT AB (http://www.bnearit.se/) 
 * Copyright 2014 UniBO (http://www.unibo.it/) 
 * 
 * This code is part of an Arrowhead System reference implementation.
 * You may use it freely within the scope of the Arrowhead project.
 * All other uses are prohibited.
 */ 
package it.unibo.arrowhead.controller;

import java.net.MalformedURLException;
import java.net.URL; /*XXX*/
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;

import se.bnearit.arrowhead.common.core.service.authorisation.AuthorisationControl;
import se.bnearit.arrowhead.common.core.service.authorisation.ws.rest.AuthorisationControlConsumerREST_WS;
import se.bnearit.arrowhead.common.core.service.authorisation.ws.rest.AuthorisationServiceTypes;
import se.bnearit.arrowhead.common.core.service.discovery.ServiceDiscovery;
import se.bnearit.arrowhead.common.core.service.discovery.dnssd.ServiceDiscoveryDnsSD;
import se.bnearit.arrowhead.common.core.service.discovery.endpoint.HttpEndpoint;
import se.bnearit.arrowhead.common.core.service.discovery.exception.ServiceRegisterException;
import se.bnearit.arrowhead.common.core.service.orchestration.OrchestrationStore;
import se.bnearit.arrowhead.common.core.service.orchestration.ws.rest.OrchestrationServiceTypes;
import se.bnearit.arrowhead.common.core.service.orchestration.ws.rest.OrchestrationStoreConsumerREST_WS;
import se.bnearit.arrowhead.common.service.ServiceEndpoint;
import se.bnearit.arrowhead.common.service.ServiceIdentity;
import se.bnearit.arrowhead.common.service.ServiceInformation;
import se.bnearit.arrowhead.common.service.ServiceMetadata;
import se.bnearit.arrowhead.common.service.exception.ServiceNotStartedException;
import se.bnearit.arrowhead.common.service.ws.rest.ClientFactoryREST_WS;
import se.bnearit.arrowhead.system.orchestration.OrchestrationMonitor;
import se.bnearit.arrowhead.system.service.AppServiceConsumer;
import se.bnearit.arrowhead.system.service.AppServiceProducer;
import se.bnearit.arrowhead.system.service.GeneralPublisher;
import se.bnearit.arrowhead.system.service.GeneralServiceProducerFactory;
import se.bnearit.arrowhead.system.service.ServiceConsumerFactory;
import se.bnearit.arrowhead.system.service.ServiceProducerFactory;
import se.bnearit.arrowhead.system.service.mapping.GeneralServiceEndpointMapper;
import se.bnearit.arrowhead.system.service.mapping.ServiceEndpointMapper;
import se.bnearit.arrowhead.system.sm.SystemManagement;
import se.bnearit.arrowhead.system.sm.exception.SystemManagementException;


/**
 * ArrowheadSystem is an Arrowhead System reference implementation.
 * The system is capable of consuming Service Discovery for service registration
 * and lookup, Authorisation Control for access control and Orchestration Store
 * for system configuration. The system is also capable of providing/consuming
 * an example application service using two TEST EVSE in the prop file.
 * To use this demo version just call it using the option --test. 
 * 
 * @author Markus.Klisics
 * @author Stradivarius
 */
public class ArrowheadSystem {
	
	private static final Logger LOG = Logger.getLogger(ArrowheadSystem.class.getName());
	/* INFO when done debugging, ALL for debugging */
	
	/* Used to connect to the ACS (parameters should be specified from command line) */
	private ServiceDiscovery serviceDiscovery;
	private AuthorisationControl authControl;
	private OrchestrationStore orchestration;
	private SystemManagement systemManagement;
	private ServiceEndpointMapper serviceEndpointMapper;
	/* List of local producers in the system */
	private List<ServiceProducerFactory> producerFactories;
	/* List of local consumers in the system */
	private List<ServiceConsumerFactory> consumerFactories;
	/* Set of allowed types of service to be consumed */
	private Set<String> supportedConsumptionServiceTypes;
	private ClientFactoryREST_WS clientFactoryREST_WS;
	private OrchestrationMonitor orchestrationMonitor;
	
	
	public ArrowheadSystem() {
		
		this.serviceEndpointMapper = new GeneralServiceEndpointMapper();
		
		configureCore();
		configureApp();
	}
	
	/* Initialize a Arrowhead System with the given name and create the log file */
	public ArrowheadSystem(ServiceEndpointMapper mapper) {
		
		this.serviceEndpointMapper = mapper;
		
		configureCore();
		configureApp();
	}
	
	/* Create an Endpoint */
	private HttpEndpoint findAndCreateEndpoint(String serviceType) {
		HttpEndpoint endpoint = null;
		if (serviceDiscovery != null) {
			List<ServiceIdentity> authServices = serviceDiscovery.getServicesByType(serviceType);
			if (authServices.size() > 0) {
				ServiceIdentity authServiceId = authServices.get(0);
				ServiceInformation authServiceInfo = serviceDiscovery.getServiceInformation(authServiceId, HttpEndpoint.ENDPOINT_TYPE);
				if (authServiceInfo != null) {
					endpoint = (HttpEndpoint) authServiceInfo.getEndpoint();
					LOG.info("Found service " + authServiceId);
				} else {
					LOG.warning("Could not get service information about " + authServiceId);
				}
			} else {
				LOG.info("Could not find any services of type" + serviceType);
			}
		}
		return endpoint;
	}

	private void configureCore() {
		/* Discover a Arrowhead Core Service according to the parameters given in input.
		 * Remember to catch the exception in case of failure! */
		serviceDiscovery = new ServiceDiscoveryDnsSD();
		
		/* Instantiate the client factory with some parameters got from the file */
		clientFactoryREST_WS = new ClientFactoryREST_WS(
				ArrowheadController.properties.getProperty("truststore.file", ArrowheadController.getSystemName() + ".jks"),
				ArrowheadController.properties.getProperty("truststore.password", "alpha1234"),
				ArrowheadController.properties.getProperty("keystore.file", ArrowheadController.getSystemName() + ".jks"),
				ArrowheadController.properties.getProperty("keystore.password", "alpha1234"));
		
		/* Instantiate the Authorisation service endpoint */
		HttpEndpoint authEndpoint = findAndCreateEndpoint(AuthorisationServiceTypes.REST_WS_AUTHORISATION_CTRL_SECURE);
		if (authEndpoint == null) {
			authEndpoint = HttpEndpoint.createFromString(ArrowheadController.properties.getProperty("authorisation.url"));
		}
		if (authEndpoint == null) {
			LOG.severe("Failed to read authorisation url");
			System.out.println("Failed to read authorisation url. Check the system configuration.");
			System.exit(-1);
		}
		LOG.info("Using authorisation control provider " + authEndpoint);

		/* Instantiate the Orchestration service endpoint */
		HttpEndpoint orchEndpoint = findAndCreateEndpoint(OrchestrationServiceTypes.REST_WS_ORCHESTRATION_STORE_SECURE);
		if (orchEndpoint == null) {
			orchEndpoint = HttpEndpoint.createFromString(ArrowheadController.properties.getProperty("orchestration.url"));
		}
		if (orchEndpoint == null) {
			LOG.severe("Failed to read orchestration url");
			System.out.println("Failed to read orchestration url. Check the system configuration.");
			System.exit(-1);
		}
		LOG.info("Using orchestration store provider " + orchEndpoint);
		
		/* Set the Authorisation and Orchestration consumers */
		authControl = new AuthorisationControlConsumerREST_WS(authEndpoint, clientFactoryREST_WS);
		orchestration = new OrchestrationStoreConsumerREST_WS(orchEndpoint, clientFactoryREST_WS);
	}
	
	private void configureApp() {
		/* These two fields seem to be useless, waiting for an usage TODO */
		// String trustStoreFile = ArrowheadController.properties.getProperty("truststore.file", ArrowheadController.getSystemName() + ".jks");
		// String trustStorePasswd = ArrowheadController.properties.getProperty("truststore.password", "alpha1234");
		String keyStoreFile = ArrowheadController.properties.getProperty("keystore.file", ArrowheadController.getSystemName() + ".jks");
		String keyStorePasswd = ArrowheadController.properties.getProperty("keystore.password", "abc1234");

		/* Setup producers globals */
		producerFactories = new ArrayList<>();
		for (ServiceProducerFactory prd : ArrowheadController.Producers) {
			prd.SetParams(keyStoreFile, keyStorePasswd, serviceDiscovery, authControl);
			producerFactories.add(prd);
		}
		
		/* Set the consumers polling interval */
		int pollInterval = 30;
		try {
			pollInterval = Integer.parseInt(ArrowheadController.properties.getProperty("service.consume.polling.interval", "30"));
		} catch (NumberFormatException e) {
			LOG.warning("Invalid number format for configuration property \"service.consume.polling.interval\"");
		}
		
		/* Setup Consumers globals */
		consumerFactories = new ArrayList<>();
		for (ServiceConsumerFactory cns : ArrowheadController.Consumers) {
			cns.SetParams(clientFactoryREST_WS, pollInterval);
			consumerFactories.add(cns);
		}

		supportedConsumptionServiceTypes = new HashSet<>();
		
		/* Parse properties to add the indicated producers and the supported consuming types */
		parseProperties(ArrowheadController.properties);

		systemManagement = new SystemManagement(
				serviceDiscovery,
				supportedConsumptionServiceTypes,
				serviceEndpointMapper,
				consumerFactories);
		
		/* Set the orchestration polling interval */
		int orchInterval = 30;
		try {
			orchInterval = Integer.parseInt(ArrowheadController.properties.getProperty("orchestration.monitor.interval", "30"));
		} catch (NumberFormatException e) {
			LOG.warning("Invalid number format for configuration property \"orchestration.monitor.interval\"");
		}
		orchestrationMonitor = new OrchestrationMonitor(orchestration, orchInterval, systemManagement);
		orchestrationMonitor.start();
	}
	
	/* Parse the properties regarding service producers and initialize the producers
	 * and insert the allowed consumer types in a keystore FIXME */
	private void parseProperties(Properties props) {
		String[] consumeSupportKeys = props.getProperty("service.consume.support").split("\\|");
		for (String key : consumeSupportKeys) {
			LOG.fine("Parsing consume support " + key);
			for (ServiceConsumerFactory scf : consumerFactories) {
				if (scf.getSupportedServiceTypes().contains(key)) {
					LOG.fine("Adding " + key + " to supported consumption service types");
					supportedConsumptionServiceTypes.add(key);
				}
			}
		}
	}

	/* PRODUCERS */
	
	/* Create a producer (publish it if the flag is true) TODO remove */
	public AppServiceProducer createProducer(String name, String type, String endpoint, String params, boolean publish, boolean general) throws ServiceRegisterException{
		AppServiceProducer producer = null;
		for (ServiceProducerFactory spf : producerFactories) {
			if ((spf.getSupportedServiceTypes().contains(type) && !general) || 
					(spf.getSupportedServiceTypes().contains("general") && general)){ // to maintain consistency
				producer = spf.createServiceProducer(name, type, endpoint, params);
				if (publish) {
					try {
						producer.publish();
					} catch (ServiceRegisterException e) {
						LOG.warning("Failed to publish service producer " + name + ". Reason: " + e.getMessage());
						throw new ServiceRegisterException(e.getMessage().toString());
					}
				}
				break;
			}
		}
		return producer;
	}
	
	/* Create a producer */
	public AppServiceProducer createProducer(String name, String type, String endpoint, String params) throws ServiceRegisterException{
		AppServiceProducer producer = null;
		for (ServiceProducerFactory spf : producerFactories) {
			if ((spf.getSupportedServiceTypes().contains(type))){
				producer = spf.createServiceProducer(name, type, endpoint, params);
				break;
			}
		}
		return producer;
	}
	
	public AppServiceProducer createPublisher(String name, String type, String endpoint, String params) throws ServiceRegisterException{
		AppServiceProducer producer = null;
		for (ServiceProducerFactory spf : producerFactories) {
			if ((spf.getSupportedServiceTypes().contains(type))){
				producer = spf.createServiceProducer(name, type, endpoint, params);				
				break;
			}
		}
		
		if (producer == null) {
			try {
				GeneralServiceProducerFactory fac = new GeneralServiceProducerFactory(Arrays.asList(type),GeneralPublisher.class);
				fac.SetParams(
						ArrowheadController.properties.getProperty("keystore.file", ArrowheadController.getSystemName() + ".jks"),
						ArrowheadController.properties.getProperty("keystore.password", "abc1234"),
						serviceDiscovery, authControl);
				producerFactories.add(fac);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return createPublisher(name, type, endpoint, params);
		}
		
		return producer;
	}
	
	/* Get all service producers */
	public ArrayList<AppServiceProducer> getAllServiceProducers () {
		ArrayList<AppServiceProducer> result = new ArrayList<AppServiceProducer>();
		for (ServiceProducerFactory spf : producerFactories)
			result.addAll(spf.getAllServiceProducers());
		return result;
	}
	
	/* Get a service producer given the name in input */
	public AppServiceProducer getServiceProducerByName (String name) {
		for (ServiceProducerFactory spf : producerFactories)
			for (AppServiceProducer producer : spf.getAllServiceProducers()){
				if (producer.getName().equals(name)) {
					/* Just to be sure that the service was launched */
					try {
						producer.getEndpoint();
					} catch (ServiceNotStartedException e) {}
					return producer;
				}	
			}
		return null;
	}
	
	/* SERVICES */
	
	/* Wrapping two ServiceDiscovery functions for parallelism */
	public List<ServiceIdentity> getAllServices () {
		return serviceDiscovery.getAllServices();
	}
	
	public List<ServiceIdentity> getServicesByType (String type) {
		return serviceDiscovery.getServicesByType(type);
	}
	
	/* This is what we call service discovery, so we properly discover a single service */
	public ServiceIdentity getServiceByName (String name) {
		for (ServiceIdentity service : serviceDiscovery.getAllServices()) {
			if (service.getId().startsWith(name))
					return service;
		}
		return null;
	}
	
	/* Unique function providing additional informations */
	private ServiceInformation serviceGetInfo (ServiceIdentity service) {
		ServiceInformation serviceInfo = null;
		String endpointType = serviceEndpointMapper.getEndpointTypeForServiceType(service.getType());
		if (endpointType != null)
			serviceInfo = serviceDiscovery.getServiceInformation(service, endpointType);
		return serviceInfo;	
	}
	
	/* If some other informations are needed: Endpoint */
	public ServiceEndpoint serviceGetEndpoint (ServiceIdentity service) {
		return serviceGetInfo(service).getEndpoint();
	}
	
	/* If some other informations are needed: Identity */
	public ServiceIdentity serviceGetIdentity (ServiceIdentity service) {
		return serviceGetInfo(service).getIdentity();
	}
	
	/* If some other informations are needed: Identity */
	public ServiceMetadata serviceGetMetadata (ServiceIdentity service) {
		return serviceGetInfo(service).getMetadata();
	}
	
	/* XXX just for the semplified API - http specific */
	public URL serviceGetCompleteUrlForResource (ServiceIdentity service, String resource) throws MalformedURLException {
		HttpEndpoint http = (HttpEndpoint)serviceGetInfo(service).getEndpoint();
		return new URL(http.toURL().toString() + "/" + resource);
	}
	
	/* CONSUMERS */
	
	public AppServiceConsumer createConsumer (String serviceName) {
		try {
			return systemManagement.consume(serviceName, false);
		} catch (SystemManagementException e) {
			return null;
		}
	}
	
	public AppServiceConsumer getConsumer (String serviceName) {
		try {
			return systemManagement.consume(serviceName, true);
		} catch (SystemManagementException e) {
			return null;
		}
	}
	
	public void destroyProducer (String producerName) {
		for (ServiceProducerFactory spf : producerFactories) {
			spf.destroyServiceProducer(producerName);
		}
	}
	
	public void eraseService (String serviceName) {
		try {
			serviceDiscovery.unpublish(getServiceByName(serviceName));
		} catch (ServiceRegisterException e) {
			LOG.severe("Failed to erase service " + serviceName + ". Reason: " + e.getMessage());
		}
		LOG.info("Erased service " + serviceName);
	}
	
	public void eraseService (ServiceIdentity service) {
		try {
			serviceDiscovery.unpublish(service);
		} catch (ServiceRegisterException e) {
			LOG.severe("Failed to erase service " + service.getId() + ". Reason: " + e.getMessage());
		}
		LOG.info("Erased service " + service.getId());
	}
	
	/* Erase all services to avoid that they remain on the server */
	private void shutDownApp() {
		for (ServiceProducerFactory spf : producerFactories) {
			spf.destroyAllProducers();
		}
		for (ServiceConsumerFactory scf : consumerFactories) {
			scf.destroyAllConsumers();
		}
        orchestrationMonitor.stop();
	}
	
	private void shutDownCore() {
		// FIXME Nothing at the moment...
	}
	
	/* It should be the final stop */
	public void stop() {
		shutDownApp();
		shutDownCore();
	}
}
