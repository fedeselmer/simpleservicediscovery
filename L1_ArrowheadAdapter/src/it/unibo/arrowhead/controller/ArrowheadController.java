/*
 * Copyright 2014 UniBO (http://www.unibo.it/) 
 * 
 * This code is part of an Arrowhead System reference implementation.
 * You may use it freely within the scope of the Arrowhead project.
 * All other uses are prohibited.
 * 
 */

package it.unibo.arrowhead.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import se.bnearit.arrowhead.system.service.GeneralServiceConsumerFactory;
import se.bnearit.arrowhead.system.service.GeneralServiceProducerFactory;
import se.bnearit.arrowhead.system.service.ServiceConsumerFactory;
import se.bnearit.arrowhead.system.service.ServiceProducerFactory;

/**
 * @author Stradivarius
 *
 */
public class ArrowheadController {
	
	private static String systemName;
	public static Properties properties;
	
	protected static final Logger LOG = Logger.getLogger(ArrowheadController.class.getName());
	private static final Level DEFAULT_LOG_LEVEL = Level.INFO;
	
	/* Network values */
	private String DnsServer;
	private String DnssdDomain;
	private String DnssdHostname;
	private String TsigFile;
	
	/* Factories */
	public static ArrayList<ServiceProducerFactory> Producers;
	public static ArrayList<ServiceConsumerFactory> Consumers;
	
	public ArrowheadController (String name) {
		systemName = name;
		loadProperties(systemName);
		configureLogging(systemName);
		
		this.DnsServer = properties.getProperty("core.server");
		this.DnssdDomain = properties.getProperty("core.domain");
		this.DnssdHostname = properties.getProperty("core.hostname");
		this.TsigFile = properties.getProperty("core.tsig", "");
		
		init();
		
		/* We need to add at least the minimal producer */
		Producers.add(new GeneralServiceProducerFactory());
	}
	
	public ArrowheadController (String name, String DnsServer, String DnssdDomain, String DnssdHostname, String TsigFile) {
		systemName = name;
		configureLogging(systemName);
		loadProperties(systemName);
		
		this.DnsServer = DnsServer;
		this.DnssdDomain = DnssdDomain;
		this.DnssdHostname = DnssdHostname;
		this.TsigFile = TsigFile;
		
		init();
	}
	
	private void init () {
		System.setProperty("dns.server", DnsServer);
		System.setProperty("dnssd.domain", DnssdDomain);
		System.setProperty("dnssd.hostname", DnssdHostname);
		if (!TsigFile.equals("")) {
			System.setProperty("dnssd.tsig", TsigFile);
		}
		
		Producers = new ArrayList<ServiceProducerFactory>();
		Consumers = new ArrayList<ServiceConsumerFactory>();
	}
	
	public static void addNewProducerBinding (List<String> supportedTypes, Class<?> producerClass) {
		try {
			Producers.add(new GeneralServiceProducerFactory(supportedTypes, producerClass));
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void addNewProducerBinding (String supportedType, Class<?> producerClass) {
		try {
			Producers.add(new GeneralServiceProducerFactory(Arrays.asList(supportedType), producerClass));
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void addNewConsumerBinding (List<String> supportedTypes, Class<?> consumerClass) {
		try {
			Consumers.add(new GeneralServiceConsumerFactory(supportedTypes, consumerClass));
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void addNewConsumerBinding (String supportedType, Class<?> consumerClass) {
		try {
			Consumers.add(new GeneralServiceConsumerFactory(Arrays.asList(supportedType), consumerClass));
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public String getDnsServer() {
		return DnsServer;
	}
	
	public String getHostname() {
		return DnssdHostname;
	}

	public static String getSystemName() {
		return systemName;
	}
	
	/** 
	 * Reads the properties from the file .properties
	 *
	 */
	private static boolean loadProperties(String systemName) {
		boolean result = false;
		/* Setting up Globals */
		String fileName = systemName + ".properties";
		
		/* Read the input properties file and set the properties */
		try {
			Properties props = new Properties();
			props.load(new FileInputStream(fileName));
			properties = props;
			result = true;
		} catch (IOException e) {
			LOG.severe("Failed to read property file " + fileName + ". Reason: " + e.getMessage());
			System.exit(-1);
		}
		return result;
	}
	
	/* Creates the new file for the logs */
	private void configureLogging(String systemName) {
		FileHandler fileHandler;
		try {
			LogManager.getLogManager().reset();
			fileHandler = new FileHandler(systemName + ".log");
			if (properties.getProperty("debug", "false").equals("true"))
				fileHandler.setLevel(Level.ALL);
			else fileHandler.setLevel(DEFAULT_LOG_LEVEL);
			fileHandler.setFormatter(new SimpleFormatter());
			Logger rootLogger = Logger.getLogger("");
			if (properties.getProperty("debug", "false").equals("true"))
				rootLogger.setLevel(Level.ALL);
			else rootLogger.setLevel(DEFAULT_LOG_LEVEL);
			rootLogger.addHandler(fileHandler);
		} catch (SecurityException | IOException e) {
			System.out.println("Failed to configure logging: " + e.getMessage());
			System.exit(-1);
		}
	}
	
}
