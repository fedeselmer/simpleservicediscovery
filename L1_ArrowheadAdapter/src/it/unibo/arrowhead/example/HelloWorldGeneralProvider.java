/*
 * Copyright 2014 UniBO (http://www.unibo.it/) 
 * 
 * This code is part of an Arrowhead System reference implementation.
 * You may use it freely within the scope of the Arrowhead project.
 * All other uses are prohibited.
 */

package it.unibo.arrowhead.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.cli.ParseException;

import it.unibo.arrowhead.controller.ArrowheadController;
import it.unibo.arrowhead.controller.ArrowheadSystem;
import se.bnearit.arrowhead.common.core.service.discovery.exception.ServiceRegisterException;
import se.bnearit.arrowhead.system.service.AppServiceProducer;

/**
 * @author Stradivarius
 *
 */
public class HelloWorldGeneralProvider {

	private static final String DEFAULT_SYSTEM_NAME = "HelloWorld";
	
	public static ArrowheadSystem arrowheadSystem;
	public static ArrowheadController arrowheadController;

	/* Producer values */
	public static String ProducerName = "hello-world";
	public static String ProducerType = "_hello-ws-http._tcp";
	public static String EndpointPrefix = "/hello";
	public static String BaseParams = "";
	public static int port = 1414;

	
	/**
	 * Application main entry point.
	 * 
	 * @param args The name of the system (optional).
	 * @throws ParseException 
	 */
	public static void main(String[] args) throws ParseException {
		
		System.out.println("HELLO WORLD ARROWHEAD DECOUPLED PROVIDER\nDeveloped by Stradivarius\n");

		String systemName = DEFAULT_SYSTEM_NAME;
		if (args.length == 1) {
			systemName = args[0];
		}
		System.out.println("System name: " + systemName + "\nInitializing Controller...");
		
		/* Initialize the Arrowhead Core Services (ACS) interface */

		arrowheadController = new ArrowheadController(systemName);
	
		System.out.println("Initializing connections to the ACS...");
		
		try {
			arrowheadSystem = new ArrowheadSystem();
			System.out.println("Connection to Arrowhead Core Service succeeded with params:"
					+ "\n\tIP address: " + System.getProperty("dns.server")
					+ "\n\tDomain: " + System.getProperty("dnssd.domain")
					+ "\n\tHostname: " + System.getProperty("dnssd.hostname")
					+ "\n");
		} catch(NoClassDefFoundError e) {
			System.out.println("No Arrowhead Core Service found with params:"
					+ "\n\tIP address: " + System.getProperty("dns.server")
					+ "\n\tDomain: " + System.getProperty("dnssd.domain")
					+ "\n\tHostname: " + System.getProperty("dnssd.hostname")
					+ "\nExiting...");
			System.exit(-1);
		} catch(NullPointerException e) {
			System.out.println("No Arrowhead Core Service found with params:"
					+ "\n\tIP address: " + System.getProperty("dns.server")
					+ "\n\tDomain: " + System.getProperty("dnssd.domain")
					+ "\n\tHostname: " + System.getProperty("dnssd.hostname")
					+ "\nExiting...");
			System.exit(-1);
		}
	
		/* Here we create and publish the service */	
		try {
			AppServiceProducer producer = arrowheadSystem.createPublisher(ProducerName,
					ProducerType,
					Integer.toString(port) + "|" + EndpointPrefix,
					BaseParams);
			producer.publish();
			
			System.out.println("Success to create service producer  " + ProducerName + "." + ProducerType + " on \n\t" +
							arrowheadController.getHostname() + ":" + port + EndpointPrefix + "... Needs to be published.");
		} catch (ServiceRegisterException e) {
			System.out.println("Failed to publish service producer " + ProducerName + "." + ProducerType + ". Reason: " + e.getMessage());
		}
		
		System.out.println("If the server is not running please start it.");
		
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String line = "";
		
		System.out.println("Listening for connections... "
				+ "(type \"quit\" to exit in order to properly unpublish services)");
		while(line.equalsIgnoreCase("quit") == false){
			try {
				Thread.sleep(1000);
				line = in.readLine();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Stopping daemons...");
		arrowheadSystem.stop();
		System.out.println("Bye");
	}
}