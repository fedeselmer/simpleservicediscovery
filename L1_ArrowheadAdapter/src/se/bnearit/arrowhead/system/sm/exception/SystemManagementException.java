/* 
 * Copyright 2014 BnearIT AB (http://www.bnearit.se/) 
 * Copyright 2014 UniBO (http://www.unibo.it/) 
 * 
 * This code is part of an Arrowhead System reference implementation.
 * You may use it freely within the scope of the Arrowhead project.
 * All other uses are prohibited.
 */
package se.bnearit.arrowhead.system.sm.exception;

/** 
* @author Markus.Klisics
* @author Stradivarius
*/
public class SystemManagementException extends Exception {

	private static final long serialVersionUID = -3205268256800323149L;

	public SystemManagementException() {
	}

	public SystemManagementException(String msg) {
		super(msg);
	}

	public SystemManagementException(Throwable e) {
		super(e);
	}
	
}
