/* 
 * Copyright 2014 BnearIT AB (http://www.bnearit.se/) 
 * Copyright 2014 UniBO (http://www.unibo.it/) 
 * 
 * This code is part of an Arrowhead System reference implementation.
 * You may use it freely within the scope of the Arrowhead project.
 * All other uses are prohibited.
 */
package se.bnearit.arrowhead.system.sm;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import se.bnearit.arrowhead.common.core.service.discovery.ServiceDiscovery;
import se.bnearit.arrowhead.common.core.service.discovery.endpoint.HttpEndpoint;
import se.bnearit.arrowhead.common.service.ServiceEndpoint;
import se.bnearit.arrowhead.common.service.ServiceIdentity;
import se.bnearit.arrowhead.common.service.ServiceInformation;
import se.bnearit.arrowhead.system.service.AppServiceConsumer;
import se.bnearit.arrowhead.system.service.ServiceConsumerFactory;
import se.bnearit.arrowhead.system.service.mapping.ServiceEndpointMapper;
import se.bnearit.arrowhead.system.sm.exception.SystemManagementException;

/** 
* @author Markus.Klisics
* @author Stradivarius
*/
public class SystemManagement {

	private static final Logger LOG = Logger.getLogger(SystemManagement.class.getName());
	
	private static int consumerIndex = 1;
	
	private ServiceDiscovery serviceDiscovery;
	private Set<String> supportedConsumptionServiceTypes;
	private ServiceEndpointMapper serviceEndpointMapper;
	private List<ServiceConsumerFactory> consumerFactories;
	
	public SystemManagement(ServiceDiscovery serviceDiscovery,
			Set<String> supportedConsumptionServiceTypes,
			ServiceEndpointMapper serviceEndpointMapper,
			List<ServiceConsumerFactory> consumerFactories) {
		this.serviceDiscovery = serviceDiscovery;
		this.supportedConsumptionServiceTypes = supportedConsumptionServiceTypes;
		this.serviceEndpointMapper = serviceEndpointMapper;
		this.consumerFactories = consumerFactories;
	}

	public AppServiceConsumer consume(String serviceName, boolean get) throws SystemManagementException {
		List<String> result = new ArrayList<>();
		AppServiceConsumer sc = null;
		
		LOG.info("Start consuming " + serviceName);
		// Find service in SR
		List<ServiceIdentity> services = serviceDiscovery.getAllServices();
		for (ServiceIdentity service : services) {
			if (service.getId().startsWith(serviceName)) {
				LOG.info("Found matching service " + service);
				// Check if we can consume service type
				if (supportedConsumptionServiceTypes.contains(service.getType())) {
					// Map service type to endpoint type
					String endpointType = serviceEndpointMapper.getEndpointTypeForServiceType(service.getType());
					if (endpointType == null) {
						throw new SystemManagementException("No mapping found for service type " + service.getType());
					}
					ServiceInformation serviceInfo;
					try {
						// Get service endpoint
						serviceInfo = serviceDiscovery.getServiceInformation(service, endpointType);
					} catch (Exception e) {
						throw new SystemManagementException(e);
					}
	
					// Find service consumer consuming endpoint
					if (get) {
						sc = findServiceConsumerByEndpoint(serviceInfo.getEndpoint());
					}
					
					if (sc == null) {
						// Find a service consumer factory supporting this service type
						for (ServiceConsumerFactory scf : consumerFactories) {
							if (scf.getSupportedServiceTypes().contains(service.getType())) {
								// Start service consumer with endpoint
								sc = scf.createServiceConsumer(String.format("cons-%02d", consumerIndex++), service.getType(), serviceInfo.getEndpoint());

								result.add(service.getId());
								break;
							}
						}
					} else {
						result.add(service.getId());
					}
				} else {
					LOG.severe("Unsupported consumption service type for service " + service);
				}
			}
		}
		return sc;
	}
	
	public List<String> stopConsuming(String serviceName) throws SystemManagementException {
		List<String> result = new ArrayList<>();
		// Find service in SR
		List<ServiceIdentity> services = serviceDiscovery.getAllServices();
		for (ServiceIdentity service : services) {
			if (service.getId().startsWith(serviceName)) {
				// Map service type to endpoint type
				String endpointType = serviceEndpointMapper.getEndpointTypeForServiceType(service.getType());
				if (endpointType == null) {
					throw new SystemManagementException("No mapping found for service type " + service.getType());
				}
				// Get service endpoint
				ServiceInformation serviceInfo = serviceDiscovery.getServiceInformation(service, endpointType);
				// Find service consumer consuming endpoint
				AppServiceConsumer sc = findServiceConsumerByEndpoint(serviceInfo.getEndpoint());
				if (sc != null) {
					sc.stop();
					LOG.info("Stopped consuming " + service.getId());
					result.add(service.getId());
				}
			}
		}
		
		return result;
	}

	private AppServiceConsumer findServiceConsumerByEndpoint(ServiceEndpoint endpoint) {
		AppServiceConsumer result = null;
		
		// Find service consumer consuming endpoint
		for (ServiceConsumerFactory scf : consumerFactories) {
			for (AppServiceConsumer sc : scf.getAllServiceConsumers()) {
				if (sc.getConfiguredEndpoint().equals(endpoint) && !sc.isConsuming()) {
					result = sc;
					break;
				}
			}
		}

		return result;
	}
	
}
