/* 
 * Copyright 2014 BnearIT AB (http://www.bnearit.se/) 
 * Copyright 2014 UniBO (http://www.unibo.it/) 
 * 
 * This code is part of an Arrowhead System reference implementation.
 * You may use it freely within the scope of the Arrowhead project.
 * All other uses are prohibited.
 */
package se.bnearit.arrowhead.system.service;

import se.bnearit.arrowhead.common.service.ServiceProducer;

/** 
* @author Markus.Klisics
* @author Stradivarius
*/
public interface AppServiceProducer extends ServiceProducer {
	
	public int getResourceId();
}
