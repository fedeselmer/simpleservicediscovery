/* 
 * Copyright 2014 UniBO (http://www.unibo.it/) 
 * 
 * This code is part of an Arrowhead System reference implementation.
 * You may use it freely within the scope of the Arrowhead project.
 * All other uses are prohibited.
 */
package se.bnearit.arrowhead.system.service;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import se.bnearit.arrowhead.common.service.ServiceEndpoint;
import se.bnearit.arrowhead.common.service.ws.rest.ClientFactoryREST_WS;
import se.bnearit.arrowhead.system.service.AppServiceConsumer;
import se.bnearit.arrowhead.system.service.ServiceConsumerFactory;
import se.bnearit.resource.ResourceAllocator;

/**
 * @author Stradivarius
 *
 */
public class GeneralServiceConsumerFactory implements ServiceConsumerFactory {

	private static final Logger LOG = Logger.getLogger(GeneralServiceConsumerFactory.class.getName());
	
	private List<String> supportedTypes;
	private List<AppServiceConsumer> consumers;

	private ClientFactoryREST_WS clientFactoryREST_WS;
	private Class<?> consumerClass;
	private Constructor<?> constructor;

	private int pollingInterval;

	public GeneralServiceConsumerFactory() {
		supportedTypes = Arrays.asList();
		consumers = new ArrayList<>();
	}
	
	public GeneralServiceConsumerFactory (List<String> supportedTypes, Class<?> consumerClass) throws ClassNotFoundException, NoSuchMethodException, SecurityException {
		this.supportedTypes = supportedTypes;
		if (AppServiceConsumer.class.isAssignableFrom(consumerClass))
			this.consumerClass = consumerClass;
		else 
			throw new ClassNotFoundException("Class does not implement correctly AppServiceConsumer.");
		this.constructor = this.consumerClass.getConstructor(String.class, ServiceEndpoint.class, ClientFactoryREST_WS.class, int.class, int.class);
		consumers = new ArrayList<>();
	}
	
	@Override
	public void SetParams (ClientFactoryREST_WS clientFactoryREST_WS, int pollingInterval) {
		this.clientFactoryREST_WS = clientFactoryREST_WS;
		this.pollingInterval = pollingInterval;
	}
	
	@Override
	public List<String> getSupportedServiceTypes() {
		return Collections.unmodifiableList(supportedTypes);
	}

	@Override
	public AppServiceConsumer createServiceConsumer(String name, String type, ServiceEndpoint endpoint) {
		AppServiceConsumer result = null;
		
		LOG.info(String.format("Trying to create service consumer with name:%s type:%s endpoint:%s", name, type, endpoint));
		
		if (supportedTypes.contains(type)) {
					try {
						result = (AppServiceConsumer) constructor.newInstance(name, endpoint, clientFactoryREST_WS, pollingInterval, ResourceAllocator.getInstance().allocateResourceId());
					} catch (InstantiationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
//					result = new HelloConsumerREST_WS(name, endpoint, clientFactoryREST_WS, pollingInterval, ResourceAllocator.getInstance().allocateResourceId());
					consumers.add(result);
			} else 
				LOG.severe(String.format("HELLO service consumer of type %s not supported (name: %s)", type, name));
		return result;
	}
		
	@Override
	public List<AppServiceConsumer> getAllServiceConsumers() {
		return Collections.unmodifiableList(consumers);
	}

	@Override
	public void destroyAllConsumers() {
		for (AppServiceConsumer cons : consumers) {
			cons.stop();
		}
		consumers.clear();
	}

}
