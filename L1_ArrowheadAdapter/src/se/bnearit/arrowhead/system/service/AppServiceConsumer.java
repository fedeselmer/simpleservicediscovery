/* 
 * Copyright 2014 BnearIT AB (http://www.bnearit.se/)
 * Copyright 2014 UniBO (http://www.unibo.it/) 
 * 
 * This code is part of an Arrowhead System reference implementation.
 * You may use it freely within the scope of the Arrowhead project.
 * All other uses are prohibited.
 */
package se.bnearit.arrowhead.system.service;

import javax.ws.rs.core.Form;

import se.bnearit.arrowhead.common.service.ServiceConsumer;

/** 
* @author Markus.Klisics
* @author Stradivarius
*/
public interface AppServiceConsumer extends ServiceConsumer {
	
	public int getResourceId();
	
	@Override
	@Deprecated
	public void start();
	
	public void lock();
	
	public void unlock();
	
	abstract public String consumeResource(String resource);
	
	abstract public String consumeResource(String resource, String params);
	
	abstract public String consumeResource(String resource, Form form);
}
