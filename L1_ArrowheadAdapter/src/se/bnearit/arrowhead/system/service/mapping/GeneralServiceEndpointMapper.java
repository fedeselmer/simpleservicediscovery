/* 
 * Copyright 2014 UniBO (http://www.unibo.it/) 
 * 
 * This code is part of an Arrowhead System reference implementation.
 * You may use it freely within the scope of the Arrowhead project.
 * All other uses are prohibited.
 */
package se.bnearit.arrowhead.system.service.mapping;

import se.bnearit.arrowhead.common.core.service.discovery.endpoint.HttpEndpoint;
import se.bnearit.arrowhead.system.service.mapping.ServiceEndpointMapper;

/** 
* @author Stradivarius
*/
public class GeneralServiceEndpointMapper implements ServiceEndpointMapper {

	public String getEndpointTypeForServiceType(String serviceType) {
		String result = null;
		
		result = HttpEndpoint.ENDPOINT_TYPE;
		
		return result;
	}
	
}
