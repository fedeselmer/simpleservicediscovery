/* 
 * Copyright 2014 BnearIT AB (http://www.bnearit.se/) 
 * Copyright 2014 UniBO (http://www.unibo.it/) 
 * 
 * This code is part of an Arrowhead System reference implementation.
 * You may use it freely within the scope of the Arrowhead project.
 * All other uses are prohibited.
 */
package se.bnearit.arrowhead.system.service;

import java.util.List;

import se.bnearit.arrowhead.common.core.service.authorisation.AuthorisationControl;
import se.bnearit.arrowhead.common.core.service.discovery.ServiceDiscovery;

/** 
* @author Markus.Klisics
* @author Stradivarius
*/
public interface ServiceProducerFactory {
	
	public void SetParams(String keyStoreFile, String keyStorePassword, ServiceDiscovery serviceDiscovery, AuthorisationControl authControl);
	
	public List<String> getSupportedServiceTypes();
	
	public AppServiceProducer createServiceProducer(String name, String type, String endpoint, String params);
	
	public List<AppServiceProducer> getAllServiceProducers();
	
	public void destroyServiceProducer(String name);
	
	public void destroyAllProducers();
	
}
