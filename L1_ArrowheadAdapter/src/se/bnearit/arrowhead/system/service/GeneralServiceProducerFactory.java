/* 
 * Copyright 2014 BnearIT AB (http://www.bnearit.se/) 
 * Copyright 2014 UniBO (http://www.unibo.it/) 
 * 
 * This code is part of an Arrowhead System reference implementation.
 * You may use it freely within the scope of the Arrowhead project.
 * All other uses are prohibited.
 */
package se.bnearit.arrowhead.system.service;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

import se.bnearit.arrowhead.common.core.service.authorisation.AuthorisationControl;
import se.bnearit.arrowhead.common.core.service.discovery.ServiceDiscovery;
import se.bnearit.arrowhead.common.service.ServiceEndpoint;
import se.bnearit.arrowhead.common.service.exception.ServiceNotStartedException;
import se.bnearit.arrowhead.common.service.ws.rest.ClientFactoryREST_WS;
import se.bnearit.arrowhead.system.service.AppServiceProducer;
import se.bnearit.arrowhead.system.service.ServiceProducerFactory;
import se.bnearit.resource.ResourceAllocator;

/** 
* @author Stradivarius
*/
public class GeneralServiceProducerFactory implements ServiceProducerFactory {

	private static final Logger LOG = Logger.getLogger(GeneralServiceProducerFactory.class.getName());

	private String keyStoreFile;
	private String keyStorePassword;
	private ServiceDiscovery serviceDiscovery;
	private AuthorisationControl authControl;
	
	private List<String> supportedTypes;
	private List<AppServiceProducer> producers;

	private Class<?> producerClass;
	private Constructor<?> constructor;
	
	public GeneralServiceProducerFactory() {
	
		supportedTypes = Arrays.asList("general");
		producers = new ArrayList<>();
	}
	
	public GeneralServiceProducerFactory (List<String> supportedTypes, Class<?> producerClass) throws ClassNotFoundException, NoSuchMethodException, SecurityException {
		this.supportedTypes = supportedTypes;
		if (AppServiceProducer.class.isAssignableFrom(producerClass))
			this.producerClass = producerClass;
		else 
			throw new ClassNotFoundException("Class does not implement correctly AppServiceConsumer.");
		this.constructor = this.producerClass.getConstructor(String.class, String.class, String.class, boolean.class, String.class, String.class, String.class, ServiceDiscovery.class, AuthorisationControl.class, int.class);
		producers = new ArrayList<>();
	}
	
	public void SetParams (String keyStoreFile, String keyStorePassword, ServiceDiscovery serviceDiscovery, AuthorisationControl authControl) {
		this.keyStoreFile = keyStoreFile;
		this.keyStorePassword = keyStorePassword;
		this.serviceDiscovery = serviceDiscovery;
		this.authControl = authControl;
	}
	
	@Override
	public List<String> getSupportedServiceTypes() {
		return Collections.unmodifiableList(supportedTypes);
	}

	@Override
	public AppServiceProducer createServiceProducer(String name, String type, String endpoint, String params) {
		AppServiceProducer result = null;
		
		LOG.info(String.format("Trying to create service producer with name:%s type:%s endpoint:%s params:%s", name, type, endpoint, params));
		
		/* All Types are supported */
		if (supportedTypes.contains(type)) {
			boolean secure = false;
			
			if (type.contains("https")) {
				secure = true;
			}
	
			try {
				result = (AppServiceProducer) constructor.newInstance(name, type, endpoint, secure, keyStoreFile, keyStorePassword, params, serviceDiscovery, authControl, ResourceAllocator.getInstance().allocateResourceId());
			} catch (InstantiationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			producers.add(result);
			
		} else
			LOG.severe(String.format("Service producer of type %s not supported (name: %s)", type, name));
		
		return result;
		
//		if (type.endsWith("https._tcp")) {
//			secure = true;
//			result = new GeneralPublisherREST_WS(name, type, endpoint, secure, keyStoreFile, keyStorePassword, params, serviceDiscovery, authControl, ResourceAllocator.getInstance().allocateResourceId());
//			producers.add(result);
//		} else if (type.endsWith("http._tcp")) {
//			secure = false;
//			result = new GeneralPublisherREST_WS(name, type, endpoint, secure, keyStoreFile, keyStorePassword, params, serviceDiscovery, authControl, ResourceAllocator.getInstance().allocateResourceId());
//			producers.add(result);
//		} else
//			LOG.severe(String.format("EVSE service producer of type %s not supported (name: %s)", type, name));
//		
//		return result;
	}

	@Override
	public List<AppServiceProducer> getAllServiceProducers() {
		return Collections.unmodifiableList(producers);
	}

	@Override
	public void destroyServiceProducer (String name) {
		for (AppServiceProducer prod : producers) {
			if (prod.getName().equals(name))	
				try {
					System.out.print("Unpublishing and destroying producer "
							+ prod.getName() + prod.getServiceType() + " on "
							+ prod.getEndpoint().toString() + " ... ");
					prod.unpublish();
					prod.stop();
					System.out.println("done!");
				} catch (ServiceNotStartedException e) {
					e.printStackTrace();
				}
		}
		producers.clear();
	}
	
	@Override
	public void destroyAllProducers() {
		for (AppServiceProducer prod : producers) {
			try {
				System.out.print("Unpublishing and destroying producer "
						+ prod.getName() + prod.getServiceType() + " on "
						+ prod.getEndpoint().toString() + " ... ");
			} catch (ServiceNotStartedException e) {
				e.printStackTrace();
			}
			prod.unpublish();
			prod.stop();
			System.out.println("done!");
		}
		producers.clear();
	}

}
