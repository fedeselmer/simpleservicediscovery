/* 
 * Copyright 2014 BnearIT AB (http://www.bnearit.se/)
 * Copyright 2014 UniBO (http://www.unibo.it/) 
 * 
 * This code is part of an Arrowhead System reference implementation.
 * You may use it freely within the scope of the Arrowhead project.
 * All other uses are prohibited.
 */
package se.bnearit.arrowhead.system.service;

/** 
* @author Markus.Klisics
* @author Stradivarius
*/
public interface Authoriser {
	
	public boolean checkAuthorisation(String dn);
	
}
