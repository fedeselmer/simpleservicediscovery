/* 
 * Copyright 2014 BnearIT AB (http://www.bnearit.se/)
 * Copyright 2014 UniBO (http://www.unibo.it/) 
 * 
 * This code is part of an Arrowhead System reference implementation.
 * You may use it freely within the scope of the Arrowhead project.
 * All other uses are prohibited.
 */
package se.bnearit.arrowhead.system.service;

import javax.ws.rs.core.Form;

import se.bnearit.arrowhead.common.service.ServiceEndpoint;
import se.bnearit.arrowhead.common.service.ws.rest.ClientFactoryREST_WS;

/** 
* @author Markus.Klisics
* @author Stradivarius
*/
public abstract class ArrowheadServiceConsumer implements AppServiceConsumer {

	protected ClientFactoryREST_WS clientFactoryREST_WS;
	protected String name;
	protected int resourceId;
	protected boolean consuming;
	protected ServiceEndpoint endpoint;
	protected int pollingInterval;
	
	public ArrowheadServiceConsumer(String name, ServiceEndpoint endpoint, ClientFactoryREST_WS clientFactoryREST_WS, int pollingInterval, int resourceId) {
		this.name = name;
		this.endpoint = endpoint;
		this.clientFactoryREST_WS = clientFactoryREST_WS;
		this.pollingInterval = pollingInterval;
		this.resourceId = resourceId;
		
		consuming = false;
	}
	
	public int getResourceId() {
		return resourceId;
	}
	
	@Override
	@Deprecated
	public void start() {
		// Basically doing nothing
	}
	
	@Override
	@Deprecated
	public void stop() {
		// Basically doing nothing
	}

	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public boolean isConsuming() {
		return consuming;
	}
	
	@Override
	public ServiceEndpoint getConfiguredEndpoint() {
		return endpoint;
	}
	
	public void lock() {
		consuming = true;
	}
	
	public void unlock() {
		consuming = false;
	}
	
	abstract public String consumeResource(String resource);
	
	abstract public String consumeResource(String resource, String params);
	
	abstract public String consumeResource(String resource, Form form);
}
