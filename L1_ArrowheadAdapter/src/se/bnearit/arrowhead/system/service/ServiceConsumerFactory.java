/* 
 * Copyright 2014 BnearIT AB (http://www.bnearit.se/) 
 * Copyright 2014 UniBO (http://www.unibo.it/) 
 *
 * This code is part of an Arrowhead System reference implementation.
 * You may use it freely within the scope of the Arrowhead project.
 * All other uses are prohibited.
 */
package se.bnearit.arrowhead.system.service;

import java.util.List;

import se.bnearit.arrowhead.common.service.ServiceEndpoint;
import se.bnearit.arrowhead.common.service.ws.rest.ClientFactoryREST_WS;

/** 
* @author Markus.Klisics
* @author Stradivarius
*/
public interface ServiceConsumerFactory {
	
	public void SetParams(ClientFactoryREST_WS clientFactoryREST_WS, int pollingInterval);

	public List<String> getSupportedServiceTypes();
	
	public AppServiceConsumer createServiceConsumer(String name, String type, ServiceEndpoint endpoint);
	
	public List<AppServiceConsumer> getAllServiceConsumers();
	
	//public void destroyServiceConsumer(String name);
	
	public void destroyAllConsumers();

}
