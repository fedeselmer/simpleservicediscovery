/* 
 * Copyright 2014 BnearIT AB (http://www.bnearit.se/) 
 * Copyright 2014 UniBO (http://www.unibo.it/) 
 * 
 * This code is part of an Arrowhead System reference implementation.
 * You may use it freely within the scope of the Arrowhead project.
 * All other uses are prohibited.
 */
package se.bnearit.arrowhead.system.service;

import java.util.logging.Logger;

import se.bnearit.arrowhead.common.core.service.authorisation.AuthorisationControl;
import se.bnearit.arrowhead.common.core.service.discovery.ServiceDiscovery;
import se.bnearit.arrowhead.common.core.service.discovery.endpoint.HttpEndpoint;
import se.bnearit.arrowhead.common.service.ws.rest.BaseProviderREST_WS;
import se.bnearit.arrowhead.system.service.Authoriser;
import se.bnearit.arrowhead.system.service.AppServiceProducer;

/** 
* @author Stradivarius
*/
public class GeneralPublisher extends BaseProviderREST_WS implements AppServiceProducer, Authoriser {
	
	private int resourceId;
	private static final Logger LOG = Logger.getLogger(GeneralPublisher.class.getName());
			
	private AuthorisationControl authControl;
	
	public GeneralPublisher(
			String name,
			String type,
			String endpoint,
			boolean secure,
			String keyStoreFile,
			String keyStorePassword,
			String params,
			ServiceDiscovery serviceDiscovery,
			AuthorisationControl authControl,
			int resourceId) {
		super(name, type, secure, keyStoreFile, keyStorePassword, LOG, serviceDiscovery);
		this.authControl = authControl;
		this.resourceId = resourceId;
		
		// Parse endpoint
		String[] endpointParts = endpoint.split("\\|");
		String hostname = endpointParts[0];
		int port = Integer.parseInt(endpointParts[1]);
		String path = endpointParts[2];
		this.endpoint = new HttpEndpoint(hostname, port, path);
		// Create REST resource
		resource = null;
	}
	
	@Override
	public int getResourceId() {
		return resourceId;
	}

	@Override
	public boolean checkAuthorisation(String dn) {
		boolean result = false;
		
		if (secure) {
			if (dn != null) {
				if (authControl != null) {
					result = authControl.isAuthorized(dn, serviceId.getType(), serviceId.getId());
				} else {
					LOG.info(name + " has no authorisation control. Default allow of " + dn);
					result = true;	// Default allow when authorisation control is not available
				}
			}
		} else {
			result = true;
		}
		
		return result;
	}
	
}
