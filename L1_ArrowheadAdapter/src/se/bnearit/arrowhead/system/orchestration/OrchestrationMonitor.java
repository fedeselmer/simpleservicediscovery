/* 
 * Copyright 2014 BnearIT AB (http://www.bnearit.se/) 
 * Copyright 2014 UniBO AB (http://www.unibo.it/)
 * 
 * This code is part of an Arrowhead System reference implementation.
 * You may use it freely within the scope of the Arrowhead project.
 * All other uses are prohibited.
 */
package se.bnearit.arrowhead.system.orchestration;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import se.bnearit.arrowhead.common.core.service.orchestration.OrchestrationStore;
import se.bnearit.arrowhead.common.core.service.orchestration.data.OrchestrationConfig;
import se.bnearit.arrowhead.system.sm.SystemManagement;
import se.bnearit.arrowhead.system.sm.exception.SystemManagementException;

/** 
* @author Markus.Klisics
* @author Stradivarius
*/
public class OrchestrationMonitor {

	private static final Logger LOG = Logger.getLogger(OrchestrationMonitor.class.getName());
	
	private OrchestrationStore orchestration;
	private int interval;
	private boolean running;
	private Thread monitor;
	private OrchestrationConfig lastConfig;

	private SystemManagement systemManagement;

	public OrchestrationMonitor(OrchestrationStore orchestration, int interval, SystemManagement systemManagement) {
		this.orchestration = orchestration;
		this.interval = interval;
		this.systemManagement = systemManagement;
		running = false;
	}
	
	public void start() {
		if (!running) {
			monitor = new Thread(new Runnable() {
				@Override
				public void run() {
					LOG.info("OrchestrationMonitor started");
					while (running) {
						checkOrchestration();
						try {
							Thread.sleep(1000*interval);
						} catch (InterruptedException e) {
						}
					}
					LOG.info("OrchestrationMonitor stopped");
				}
			});
			running = true;
			monitor.start();
		}
	}
	
	public void stop() {
		if (running) {
			try {
				running = false;
				monitor.interrupt();
				monitor.join(5000);
			} catch (InterruptedException e) {
			}
		}
		
	}
	
	private void checkOrchestration() {
		LOG.fine("Checking for a new/updated active orchestration configuration");
		OrchestrationConfig activeCfg = orchestration.getActiveConfiguration();
		if (activeCfg != null) {
			LOG.fine("Found active orchestration configuration: " + activeCfg.getName());
			if (lastConfig == null || !lastConfig.getName().equals(activeCfg.getName()) || lastConfig.getSerialNumber() < activeCfg.getSerialNumber()) {
				LOG.fine("Detected an updated active orchestration config: " + activeCfg.toString());
				List<String> oldRules = new ArrayList<>();
				List<String> newRules = activeCfg.getRules();
				if (lastConfig != null) {
					oldRules = lastConfig.getRules();
				}
				
				// Apply new/updated config
				List<String> toRemove = new ArrayList<>(oldRules);
				toRemove.removeAll(newRules);
				for (String rule : toRemove) {
					LOG.info("Removing consumption according to orchestration rule: " + rule);
					try {
						systemManagement.stopConsuming(rule);
					} catch (SystemManagementException e) {
						LOG.warning("Failed to stop consuming " + rule + ". Reason: " + e.getMessage());
					}
				}
				
				List<String> toAdd = new ArrayList<>(newRules);
				toAdd.removeAll(oldRules);
				for (String rule : toAdd) {
					LOG.info("Adding consumption according to orchestration rule: " + rule);
					try {
						systemManagement.consume(rule, true);
					} catch (SystemManagementException e) {
						LOG.warning("Failed to start consuming " + rule + ". Reason: " + e.getMessage());
					}
				}
				
				lastConfig = activeCfg;
			}
		} else {
			LOG.fine("No active orchestration configuration found");
		}
	}
	
}
