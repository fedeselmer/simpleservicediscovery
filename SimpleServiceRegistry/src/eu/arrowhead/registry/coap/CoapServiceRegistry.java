package eu.arrowhead.registry.coap;

import java.io.StringReader;
import java.io.StringWriter;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.CoapServer;
import org.eclipse.californium.core.coap.CoAP.ResponseCode;
import org.eclipse.californium.core.coap.MediaTypeRegistry;
import org.eclipse.californium.core.network.CoAPEndpoint;
import org.eclipse.californium.core.server.resources.CoapExchange;
import org.eclipse.californium.core.server.resources.Resource;

import com.google.gson.Gson;
import se.bnearit.arrowhead.common.core.service.discovery.endpoint.HttpEndpoint;
import se.bnearit.arrowhead.common.core.service.discovery.exception.ServiceRegisterException;
import se.bnearit.arrowhead.common.service.ServiceIdentity;
import se.bnearit.arrowhead.common.service.ServiceMetadata;
import se.bnearit.arrowhead.system.service.AppServiceProducer;
import eu.arrowhead.registry.SimpleServiceRegistry;
import eu.arrowhead.registry.data.Properties;
import eu.arrowhead.registry.data.Property;
import eu.arrowhead.registry.data.Service;
import eu.arrowhead.registry.data.ServiceList;
import eu.arrowhead.registry.data.ServiceTypeList;

public class CoapServiceRegistry implements Runnable {

	CoapService coapService;
	public CoapServiceRegistry(InetSocketAddress address) {
		coapService = new CoapService(address);
	}
	
	@Override
	public void run() {
		//start the Coap Server
		coapService.start();
	}
	
	public void end() {
		coapService.stop();
	}

	
	class CoapService extends CoapServer {
		
		public CoapService (InetSocketAddress serviceAddress ) {
			
			CoAPEndpoint cep = new CoAPEndpoint(serviceAddress);
			
			CoapResource root = new CoapResource("serviceregistry");
			
			CoapResource serviceResource = new CoapResource("service") {
				
				@Override
				public Resource getChild(String name) {
					return this;
				}
				
				@Override
				public void handleGET(CoapExchange exchange) {
					String responsePayload = null;
					int requestedContentFormat = exchange.getRequestOptions().getContentFormat();	
					
					String urlpath = exchange.advanced().getRequest().getURI();
					System.out.println(urlpath);
					String name = urlpath.substring(urlpath.lastIndexOf("/") + 1);
					System.out.println(name + " " + this.getName());
					
					if (name.equals(this.getName())) {
						List<Service> serviceList = new ArrayList<Service>();
						List<ServiceIdentity> services = SimpleServiceRegistry.arrowheadSystem.getAllServices();
						for (ServiceIdentity id : services) {
							Service service = new Service();
							service.setName(id.getId());
							service.setType(id.getType());
							HttpEndpoint httpEndpoint = (HttpEndpoint)SimpleServiceRegistry.arrowheadSystem.serviceGetEndpoint(id);
							
							service.setHost(httpEndpoint.getHost());
							service.setDomain(httpEndpoint.getHost().replaceFirst("\\w+\\.", ""));
							service.setPort(httpEndpoint.getPort());
							List<Property> properties = new ArrayList<Property>();
							ServiceMetadata metadata = SimpleServiceRegistry.arrowheadSystem.serviceGetMetadata(id);
							for (String key : metadata.keySet()) {
								properties.add(new Property(
										key,
										metadata.get(key)
										));
							}

							String endp = SimpleServiceRegistry.arrowheadSystem.serviceGetEndpoint(id).toString();
							
							Pattern pattern = Pattern.compile("path=(/.+),");
							Matcher matcher = pattern.matcher(endp);
							if(matcher.find()) {
								properties.add(new Property(
										"path",
										matcher.group(1)
										));

							}
							
							service.setProperties(new Properties(properties));
							serviceList.add(service);
						}
						ServiceList serviceListObject = new ServiceList(serviceList);	
						
						// serialise to xml
						responsePayload = serializeObject(serviceListObject, requestedContentFormat);
						
					} else {
						
						ServiceIdentity serviceIdentity = SimpleServiceRegistry.arrowheadSystem.getServiceByName(name);
						Service service = new Service();
						if (serviceIdentity != null) {
	
							service.setName(serviceIdentity.getId());
							service.setType(serviceIdentity.getType());
							HttpEndpoint httpEndpoint = (HttpEndpoint)SimpleServiceRegistry.arrowheadSystem.serviceGetEndpoint(serviceIdentity);
							
							service.setHost(httpEndpoint.getHost());
							service.setDomain(httpEndpoint.getHost().replaceFirst("\\w+\\.", ""));
							service.setPort(httpEndpoint.getPort());
							List<Property> properties = new ArrayList<Property>();
							ServiceMetadata metadata = SimpleServiceRegistry.arrowheadSystem.serviceGetMetadata(serviceIdentity);
							for (String key : metadata.keySet()) {
								properties.add(new Property(
										key,
										metadata.get(key)
										));
							}
							
							String endp = SimpleServiceRegistry.arrowheadSystem.serviceGetEndpoint(serviceIdentity).toString();
							
							Pattern pattern = Pattern.compile("path=(/.+),");
							Matcher matcher = pattern.matcher(endp);
							if(matcher.find()) {
								properties.add(new Property(
										"path",
										matcher.group(1)
										));
		
							}
							
							service.setProperties(new Properties(properties));						
						}
						// serialise to xml
						responsePayload = serializeObject(service, requestedContentFormat);
					}
					// output string to console
					if (responsePayload != null) {
						System.out.println(responsePayload);					
						exchange.respond(ResponseCode.CONTENT,responsePayload, requestedContentFormat);						
					} else {
						System.out.println(responsePayload);					
						exchange.respond(ResponseCode.UNSUPPORTED_CONTENT_FORMAT);	
					}
					
				}
				
			};
					
			CoapResource typeResource = new CoapResource("type") {
				@Override
				public Resource getChild(String name) {
					return this;
				}
				
				@Override
				public void handleGET(CoapExchange exchange) {
					
					String responsePayload = null;
					int requestedContentFormat = exchange.getRequestOptions().getContentFormat();						
					
					String urlpath = exchange.advanced().getRequest().getURI();
					System.out.println(urlpath);
					String type = urlpath.substring(urlpath.lastIndexOf("/") + 1);
					System.out.println(type + " " + this.getName());
					
					
					if (type.equals(this.getName())) {
					
						List<ServiceIdentity> services = SimpleServiceRegistry.arrowheadSystem.getAllServices();
						ArrayList<String> types = new ArrayList<String>();
						
						for (ServiceIdentity id : services)
							if (!types.contains(id.getType()))
								types.add(id.getType());
						
						ServiceTypeList typeList = new ServiceTypeList(types);	
						
						// serialise to xml
						responsePayload = serializeObject(typeList, requestedContentFormat);
					} else {
						List<Service> serviceList = new ArrayList<Service>();
						List<ServiceIdentity> services = SimpleServiceRegistry.arrowheadSystem.getServicesByType(type);
						for (ServiceIdentity id : services) {
							Service service = new Service();
							service.setName(id.getId());
							service.setType(id.getType());
							HttpEndpoint httpEndpoint = (HttpEndpoint)SimpleServiceRegistry.arrowheadSystem.serviceGetEndpoint(id);
							
							service.setHost(httpEndpoint.getHost());
							service.setDomain(httpEndpoint.getHost().replaceFirst("\\w+\\.", ""));
							service.setPort(httpEndpoint.getPort());
							List<Property> properties = new ArrayList<Property>();
							ServiceMetadata metadata = SimpleServiceRegistry.arrowheadSystem.serviceGetMetadata(id);
							for (String key : metadata.keySet()) {
								System.out.println(metadata.size());
								properties.add(new Property(
										key,
										metadata.get(key)
										));
							}

							String endp = SimpleServiceRegistry.arrowheadSystem.serviceGetEndpoint(id).toString();
							
							Pattern pattern = Pattern.compile("path=(/.+),");
							Matcher matcher = pattern.matcher(endp);
							if(matcher.find()) {
								properties.add(new Property(
										"path",
										matcher.group(1)
										));
		
							}
							
							
							service.setProperties(new Properties(properties));
							serviceList.add(service);
						}
						ServiceList result = new ServiceList(serviceList);
						
						// serialise to xml
						responsePayload = serializeObject(result, requestedContentFormat);
					}
					// output string to console
					if (responsePayload != null) {
						System.out.println(responsePayload);
						exchange.respond(ResponseCode.CONTENT, responsePayload, requestedContentFormat);
					} else {
						System.out.println("Error, no payload to be sent back");
						exchange.respond(ResponseCode.UNSUPPORTED_CONTENT_FORMAT);
						
					}
				}
			};
			
					
			
			CoapResource publishResource = new CoapResource("publish") {
								
				@Override
				public void handlePOST(CoapExchange exchange) {
										
					boolean authentication = true;	
					boolean error = false;
					
					int requestedContentFormat = exchange.getRequestOptions().getContentFormat();
					String deserializedObject= exchange.getRequestText();
					Service service = (Service) deserializeOject(Service.class, deserializedObject, requestedContentFormat);
					if (service != null) {
						String servicePath = null;
						for(Property property : service.getProperties().getProperty()) {
							if(property.getName().equals("path")) {
								servicePath = property.getValue();
							}
						}
						
						AppServiceProducer publisher;
						try {
							publisher = SimpleServiceRegistry.arrowheadSystem.createPublisher(service.getName(), service.getType(), service.getPort() + "|" + servicePath, "some=else");
							publisher.publish();
						} catch (ServiceRegisterException e) {
							authentication = false;
						}
					} else {
						error = true;
					}
					
					if (!authentication) {
						exchange.respond(ResponseCode.FORBIDDEN);
					} else if (error) {
						exchange.respond(ResponseCode.BAD_REQUEST);
					} else {					
						exchange.respond(ResponseCode.VALID);
					}
				}
			};
			
			
			
			CoapResource unpublishResource = new CoapResource("unpublish") {
				@Override
				public Resource getChild(String name) {
					return this;
				}
				
				@Override
				public void handlePOST(CoapExchange exchange) {
					
					boolean authentication = true;
					boolean error = false;
					
					int requestedContentFormat = exchange.getRequestOptions().getContentFormat();
					String deserializedObject= exchange.getRequestText();
					Service service = (Service) deserializeOject(Service.class, deserializedObject, requestedContentFormat);
					if (service != null) {
						String serviceName = service.getName();					
						SimpleServiceRegistry.arrowheadSystem.eraseService(serviceName);
					} else {
						error = true;
					}
					
					if (!authentication) {
						exchange.respond(ResponseCode.FORBIDDEN);
					} else if (error) {
						exchange.respond(ResponseCode.BAD_REQUEST);
					} else {					
						exchange.respond(ResponseCode.VALID);
					}
					
				}
			};
						
			
			
						
			this.addEndpoint(cep);
			this.add(root);
			root.add(serviceResource);
			root.add(typeResource);
			root.add(publishResource);
			root.add(unpublishResource);
							
		}
		
		public Object deserializeOject(Class<?> objectClass, String serializedObject, int format) {
		//public Object deserializeOject(String className, String serializedObject, int format) {
			Object objt = null;
			
			if (format == MediaTypeRegistry.APPLICATION_XML) {
				JAXBContext context;
				try {
					context = JAXBContext.newInstance(objectClass);
					//context = JAXBContext.newInstance(c);
				           
					javax.xml.bind.Unmarshaller m = context.createUnmarshaller();
					objt = m.unmarshal(new StringReader(serializedObject));
				} catch (JAXBException e) {
					e.printStackTrace();
				}
				return objt;
			} else if (format == MediaTypeRegistry.APPLICATION_JSON) {
				try {
					objt = new Gson().fromJson(serializedObject, objectClass);
				} catch(Exception e) {
					e.printStackTrace();
				}
				return objt;
			} else if (format == MediaTypeRegistry.APPLICATION_LINK_FORMAT) {
				return null;
			} else {
				return null;
			}
		}
		
		public String serializeObject(Object objt, int format) {

			if (format == MediaTypeRegistry.APPLICATION_XML) {
				StringWriter writer = new StringWriter();
				JAXBContext context;
				try {
					context = JAXBContext.newInstance(objt.getClass());
				           
					Marshaller m = context.createMarshaller();
					m.marshal(objt, writer);
				} catch (JAXBException e) {
					e.printStackTrace();
				}
				return writer.toString();
			} else if (format == MediaTypeRegistry.APPLICATION_JSON) {
				return new Gson().toJson(objt);
			} else if (format == MediaTypeRegistry.APPLICATION_LINK_FORMAT) {
				return null;
			} else {
				return null;
			}
		}
	}
}





