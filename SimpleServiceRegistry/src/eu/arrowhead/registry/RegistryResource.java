/* 
 * Copyright 2014 UniBO (http://www.unibo.it/) 
 * 
 * This code is part of an Arrowhead System reference implementation.
 * You may use it freely within the scope of the Arrowhead project.
 * All other uses are prohibited.
 */

package eu.arrowhead.registry;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.eclipse.californium.core.coap.CoAP.ResponseCode;

import eu.arrowhead.registry.data.Properties;
import eu.arrowhead.registry.data.Property;
import eu.arrowhead.registry.data.Service;
import eu.arrowhead.registry.data.ServiceList;
import eu.arrowhead.registry.data.ServiceTypeList;
import se.bnearit.arrowhead.common.core.service.discovery.endpoint.HttpEndpoint;
import se.bnearit.arrowhead.common.core.service.discovery.exception.ServiceRegisterException;
import se.bnearit.arrowhead.common.service.ServiceIdentity;
import se.bnearit.arrowhead.common.service.ServiceMetadata;
import se.bnearit.arrowhead.system.service.AppServiceProducer;

/**
 * @author Stradivarius
 *
 */
@Path("/")
@Singleton
public class RegistryResource {
	
	public RegistryResource() {
	}

	/**
	 * Get all services
	 */
	@Path("/service")
	@GET
    @Produces(MediaType.APPLICATION_XML)
    public Response getServices(@Context HttpServletRequest request) {
		Response response;

		List<Service> serviceList = new ArrayList<Service>();
		List<ServiceIdentity> services = SimpleServiceRegistry.arrowheadSystem.getAllServices();
		for (ServiceIdentity id : services) {
			Service service = new Service();
			service.setName(id.getId());
			service.setType(id.getType());
			HttpEndpoint httpEndpoint = (HttpEndpoint)SimpleServiceRegistry.arrowheadSystem.serviceGetEndpoint(id);
			
			service.setHost(httpEndpoint.getHost());
			service.setDomain(httpEndpoint.getHost().replaceFirst("\\w+\\.", ""));
			service.setPort(httpEndpoint.getPort());
			List<Property> properties = new ArrayList<Property>();
			ServiceMetadata metadata = SimpleServiceRegistry.arrowheadSystem.serviceGetMetadata(id);
			for (String key : metadata.keySet()) {
				properties.add(new Property(
						key,
						metadata.get(key)
						));
			}

			String endp = SimpleServiceRegistry.arrowheadSystem.serviceGetEndpoint(id).toString();
			
			Pattern pattern = Pattern.compile("path=(/.+),");
			Matcher matcher = pattern.matcher(endp);
			if(matcher.find()) {
				properties.add(new Property(
						"path",
						matcher.group(1)
						));

			}
			
			service.setProperties(new Properties(properties));
			serviceList.add(service);
		}
		ServiceList result = new ServiceList(serviceList);	
		response = Response.ok(result).build();
		return response;
    }
	
	/**
	 * Get a service record given the name
	 */
	@Path("/service/{name}")
	@GET
    @Produces(MediaType.APPLICATION_XML)
    public Response getServiceByName(@Context HttpServletRequest request, @PathParam("name") String name) {
		Response response;
		
		ServiceIdentity serviceIdentity = SimpleServiceRegistry.arrowheadSystem.getServiceByName(name);
		Service service = new Service();
		service.setName(serviceIdentity.getId());
		service.setType(serviceIdentity.getType());
		HttpEndpoint httpEndpoint = (HttpEndpoint)SimpleServiceRegistry.arrowheadSystem.serviceGetEndpoint(serviceIdentity);
		
		service.setHost(httpEndpoint.getHost());
		service.setDomain(httpEndpoint.getHost().replaceFirst("\\w+\\.", ""));
		service.setPort(httpEndpoint.getPort());
		List<Property> properties = new ArrayList<Property>();
		ServiceMetadata metadata = SimpleServiceRegistry.arrowheadSystem.serviceGetMetadata(serviceIdentity);
		for (String key : metadata.keySet()) {
			properties.add(new Property(
					key,
					metadata.get(key)
					));
		}
		
		String endp = SimpleServiceRegistry.arrowheadSystem.serviceGetEndpoint(serviceIdentity).toString();
		
		Pattern pattern = Pattern.compile("path=(/.+),");
		Matcher matcher = pattern.matcher(endp);
		if(matcher.find()) {
			properties.add(new Property(
					"path",
					matcher.group(1)
					));

		}
		
		service.setProperties(new Properties(properties));
		
		response = Response.ok(service).build();
		return response;
	}
	
	/**
	 * Get all Service Types
	 */
	@Path("/type")
	@GET
	@Produces(MediaType.APPLICATION_XML)
	public Response getServiceTypes(@Context HttpServletRequest request) {
		Response response;
		
		List<ServiceIdentity> services = SimpleServiceRegistry.arrowheadSystem.getAllServices();
		ArrayList<String> types = new ArrayList<String>();
		
		for (ServiceIdentity id : services)
			if (!types.contains(id.getType()))
				types.add(id.getType());
		
		ServiceTypeList typeList = new ServiceTypeList(types);	
		response = Response.ok(typeList).build();
		
		return response;
	}
	
	/**
	 * Get all Services for a specified type
	 */
	@Path("/type/{type}")
	@GET
    @Produces(MediaType.APPLICATION_XML)
    public Response getServicesByType(@Context HttpServletRequest request, @PathParam("type") String type) {
		Response response;
		
		List<Service> serviceList = new ArrayList<Service>();
		List<ServiceIdentity> services = SimpleServiceRegistry.arrowheadSystem.getServicesByType(type);
		for (ServiceIdentity id : services) {
			Service service = new Service();
			service.setName(id.getId());
			service.setType(id.getType());
			HttpEndpoint httpEndpoint = (HttpEndpoint)SimpleServiceRegistry.arrowheadSystem.serviceGetEndpoint(id);
			
			service.setHost(httpEndpoint.getHost());
			service.setDomain(httpEndpoint.getHost().replaceFirst("\\w+\\.", ""));
			service.setPort(httpEndpoint.getPort());
			List<Property> properties = new ArrayList<Property>();
			ServiceMetadata metadata = SimpleServiceRegistry.arrowheadSystem.serviceGetMetadata(id);
			for (String key : metadata.keySet()) {
				System.out.println(metadata.size());
				properties.add(new Property(
						key,
						metadata.get(key)
						));
			}
			
			String endp = SimpleServiceRegistry.arrowheadSystem.serviceGetEndpoint(id).toString();
			
			Pattern pattern = Pattern.compile("path=(/.+),");
			Matcher matcher = pattern.matcher(endp);
			if(matcher.find()) {
				properties.add(new Property(
						"path",
						matcher.group(1)
						));
			}
			
			
			service.setProperties(new Properties(properties));
			serviceList.add(service);
		}
		ServiceList result = new ServiceList(serviceList);
		
		response = Response.ok(result).build();
		return response;
    }

	/**
	 * Publish a new service
	 */
	@Path("/publish")
	@POST
	@Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
//    public Response postPublish(@Context HttpServletRequest request,
//    		@FormParam("serviceName") String serviceName, /* Not the ID */
//    		@FormParam("serviceType") String serviceType,
//    		@FormParam("endPoint") String endPoint,
//    		@FormParam("params") String params
//    		) {
    public Response postPublish(Service service) {
		
		Response response;
		boolean authentication = true;
		boolean error = false;		
		
		if (service != null) {
			String servicePath = null;
			for(Property property : service.getProperties().getProperty()) {
				if(property.getName().equals("path")) {
					servicePath = property.getValue();
				}
			}
			
			AppServiceProducer publisher;
			try {
				publisher = SimpleServiceRegistry.arrowheadSystem.createPublisher(service.getName(), service.getType(), service.getHost() + "|" + service.getPort() + "|" + servicePath, "some=else");
				publisher.publish();
			} catch (ServiceRegisterException e) {
				authentication = false;
			}
		} else {
			error = true;
		}
		
		if (!authentication) {
			response = Response.status(Response.Status.FORBIDDEN).build();
		} else if (error) {
			response = Response.status(Response.Status.BAD_REQUEST).build();
		} else {					
			response = Response.ok().build();
		}
		
		
//		AppServiceProducer publisher;
//		try {
//			publisher = SimpleServiceRegistry.arrowheadSystem.createPublisher(service.getName(), service.getType(), service.getHost(), params);
//			publisher.publish();
//		} catch (Exception e) {
//			authentication = false;
//		}
		
//		String confirm = "<confirm>" + authentication + "</confirm>";
//		
//		if (!authentication) {
//			response = Response.status(Response.Status.FORBIDDEN).build();
//		} else {
//			response = Response.ok(confirm).build();
//		}
		return response;
    }
		
	/**
	 * Unpublish a given service
	 */
	/**
	 * Publish a new service
	 */
	@Path("/unpublish")
	@POST
	@Consumes(MediaType.APPLICATION_XML)
    @Produces(MediaType.APPLICATION_XML)
    public Response deleteUnublish(Service service) {
		Response response;
		boolean authentication = true;
		
		SimpleServiceRegistry.arrowheadSystem.eraseService(service.getName());
		
		
		if (!authentication) {
			response = Response.status(Response.Status.FORBIDDEN).build();
		} else {
			response = Response.ok().build();
		}
		return response;
    }
	
}