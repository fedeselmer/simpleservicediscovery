/*
 * Copyright 2014 UniBO (http://www.unibo.it/) 
 * 
 * This code is part of an Arrowhead System reference implementation.
 * You may use it freely within the scope of the Arrowhead project.
 * All other uses are prohibited.
 */

package eu.arrowhead.registry;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Properties;

import it.unibo.arrowhead.controller.ArrowheadController;
import it.unibo.arrowhead.controller.ArrowheadSystem;

import org.apache.commons.cli.ParseException;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import eu.arrowhead.registry.coap.CoapServiceRegistry;

/**
 * @author Stradivarius
 *
 */

public class SimpleServiceRegistry {

	/* Producer values */
	public static String EndpointPrefix = "/servicediscovery";
	public static int port = 8045;
    public static ArrowheadSystem arrowheadSystem;
    public static ArrowheadController arrowheadController;
	public static Properties properties;
	
	/**
	 * Application main entry point.
	 * 
	 * @param args The name of the system (optional).
	 * @throws ParseException 
	 */
	public static void main(String[] args) throws Exception {
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath(EndpointPrefix);
        
		loadProperties("SimpleServiceRegistry");
        
        /* Instantiate the DNS.SD connection to the actual Service Registry */
        arrowheadController = new ArrowheadController("SimpleServiceRegistry");

        try {
			arrowheadSystem = new ArrowheadSystem();
			System.out.println("Connection to Arrowhead Core Service succeeded with params:"
					+ "\n\tIP address: " + System.getProperty("dns.server")
					+ "\n\tDomain: " + System.getProperty("dnssd.domain")
					+ "\n\tHostname: " + System.getProperty("dnssd.hostname")
					+ "\n");
		} catch(NoClassDefFoundError e) {
			System.out.println("No Arrowhead Core Service found with params:"
					+ "\n\tIP address: " + System.getProperty("dns.server")
					+ "\n\tDomain: " + System.getProperty("dnssd.domain")
					+ "\n\tHostname: " + System.getProperty("dnssd.hostname")
					+ "\nExiting...");
			System.exit(-1);
		} catch(NullPointerException e) {
			System.out.println("No Arrowhead Core Service found with params:"
					+ "\n\tIP address: " + System.getProperty("dns.server")
					+ "\n\tDomain: " + System.getProperty("dnssd.domain")
					+ "\n\tHostname: " + System.getProperty("dnssd.hostname")
					+ "\nExiting...");
			System.exit(-1);
		}
        
        Server jettyServer = new Server(port);
        jettyServer.setHandler(context);
 
        ServletHolder jerseyServlet = context.addServlet(
             org.glassfish.jersey.servlet.ServletContainer.class, "/*");
        jerseyServlet.setInitOrder(0);
 
        // Tells the Jersey Servlet which REST service/class to load.
        jerseyServlet.setInitParameter(
           "jersey.config.server.provider.classnames",
           RegistryResource.class.getCanonicalName());
 
        String ipaddress = properties.getProperty("simpleservicediscovery.ip");
        int coapPort = Integer.parseInt(properties.getProperty("simpleservicediscovery.coap.port"));
        
        CoapServiceRegistry coapServiceRegistry = new CoapServiceRegistry(new InetSocketAddress(ipaddress, coapPort));
        Thread coapServiceThread = new Thread(coapServiceRegistry);
        
        
        try {
        	coapServiceThread.start();
            jettyServer.start();
            jettyServer.join();
        } finally {
            jettyServer.destroy();
            coapServiceRegistry.end();
        }
    }
	
	/** 
	 * Reads the properties from the file .properties
	 *
	 */
	private static boolean loadProperties(String systemName) {
		boolean result = false;
		/* Setting up Globals */
		String fileName = systemName + ".properties";
		
		/* Read the input properties file and set the properties */
		try {
			Properties props = new Properties();
			props.load(new FileInputStream(fileName));
			properties = props;
			result = true;
		} catch (IOException e) {
			//LOG.severe("Failed to read property file " + fileName + ". Reason: " + e.getMessage());
			System.exit(-1);
		}
		return result;
	}
	
	
}